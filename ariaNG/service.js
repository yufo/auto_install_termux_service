'use strict';
const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const send = require('koa-send');
const fs = require('fs');
const path = require('path');

const app = new Koa();
app.use(bodyParser());
app.use(async function(ctx, next) {
  const paths = ctx.path;
  const dirPath = path.resolve(__dirname, '.' + paths);
  if (fs.existsSync(dirPath)) {
    await send(ctx, path.resolve(__dirname, dirPath), { root: '/', index: 'index.html', hidden: true })
  } else {
    console.log('找不到资源->' + dirPath);
    // await send(ctx, path.resolve(__dirname, './system/index.html'), { root: '/', index: 'index.html', hidden: true })
  }
});

app.listen(4001);
console.log('静态服务启动成功')
