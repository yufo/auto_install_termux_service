module.exports = {
  apps: [
    {
      name: 'frp',
      cwd: '{{homePath}}/frp', // 工作目录
      script: './frpc -c ./frpc.ini', // 执行脚本
      args: '', // 传递参数 array 或者 string
      interpreter: '', // 解释器路径, 默认 node
      env: '', // 环境
      exec_mode: 'fork', // 执行模式 fork|进程  cluster|集群
      instances: 1, // 集群, 不确定支持不支持 max
      watch: ['./frpc.ini'], // 监听文件变动
      ignore_watch: '', // 忽略 watch 的 array 或者 string
    },
    {
      name: 'shell',
      cwd: '{{homePath}}/', // 工作目录
      script: 'shell', // 执行脚本
      args: '', // 传递参数 array 或者 string
      interpreter: '', // 解释器路径, 默认 node
      env: '', // 环境
      exec_mode: 'cluster', // 执行模式 fork|进程  cluster|集群
      instances: 'max', // 集群, 不确定支持不支持 max
      watch: false, // 监听文件变动
      ignore_watch: '', // 忽略 watch 的 array 或者 string
    },
    {
      name: 'ariaNG',
      cwd: '{{homePath}}/ariaNG', // 工作目录
      script: 'service.js', // 执行脚本
      args: '', // 传递参数 array 或者 string
      interpreter: 'node', // 解释器路径, 默认 node
      env: '', // 环境
      exec_mode: 'cluster', // 执行模式 fork|进程  cluster|集群
      instances: 'max', // 集群, 不确定支持不支持 max
      watch: true, // 监听文件变动
      ignore_watch: ['node_modules'], // 忽略 watch 的 array 或者 string
    },
    {
      name: 'aria2s',
      cwd: '{{homePath}}/config.d', // 工作目录
      script: 'aria2c --conf-path=/root/config.d/aria2.conf', // 执行脚本
      args: '', // 传递参数 array 或者 string
      interpreter: '', // 解释器路径, 默认 node
      max_restarts: 1,
      env: '', // 环境
      exec_mode: 'fork', // 执行模式 fork|进程  cluster|集群
      instances: 1, // 集群, 不确定支持不支持 max
      watch: ['./aria2.conf'], // 监听文件变动
      ignore_watch: [], // 忽略 watch 的 array 或者 string
    },
    {
      name: 'dlna',
      cwd: '{{homePath}}/config.d', // 工作目录
      script: './minidlna.sh', // 执行脚本
      args: '', // 传递参数 array 或者 string
      interpreter: '', // 解释器路径, 默认 node
      max_restarts: 150,
      env: '', // 环境
      exec_mode: '', // 执行模式 fork|进程  cluster|集群
      instances: 1, // 集群, 不确定支持不支持 max
      watch: false, // 监听文件变动
      ignore_watch: [], // 忽略 watch 的 array 或者 string
    },
    {
      name: '破解',
      cwd: '{{homePath}}/decode', // 工作目录
      script: 'service.js', // 执行脚本
      args: '', // 传递参数 array 或者 string
      interpreter: 'node', // 解释器路径, 默认 node
      env: '', // 环境
      exec_mode: 'cluster', // 执行模式 fork|进程  cluster|集群
      instances: 'max', // 集群, 不确定支持不支持 max
      watch: true, // 监听文件变动
      ignore_watch: ['node_modules'], // 忽略 watch 的 array 或者 string
    },
    {
      name: '云',
      cwd: '{{homePath}}/filebrowser', // 工作目录
      script: './filebrowser -r / --port 4002', // 执行脚本
      args: '', // 传递参数 array 或者 string
      interpreter: '', // 解释器路径, 默认 node
      env: '', // 环境
      exec_mode: 'fork', // 执行模式 fork|进程  cluster|集群
      instances: 1, // 集群, 不确定支持不支持 max
      watch: false, // 监听文件变动
      ignore_watch: [], // 忽略 watch 的 array 或者 string
    }
  ]
}
