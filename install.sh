# 获取当前目录
dir=pwd
home=./
nvm_path="https://github.com/nvm-sh/nvm.git"

# 安装对应依赖
function checkDep () {
    if [ ! -d ${home} ]; then
        mkdir ${home};
    fi
    # 安装 git
    if ! type git >/dev/null 2>&1; then
        echo '检测到系统没有安装 git, 即将为您安装 git';
        apt-get install git;
    fi
    # 安装 wget
    if ! type  wget >/dev/null 2>&1; then
        echo '检测到系统没有安装 wget, 即将为您安装 wget';
        apt-get install wget;
    fi
    # 安装 nginx
    if ! type  nginx >/dev/null 2>&1; then
        echo '检测到系统没有安装 nginx, 即将为您安装 nginx';
        apt-get install nginx;
    fi
    # 安装 aria2
    if ! type  aria2c >/dev/null 2>&1; then
        echo '检测到系统没有安装 aria2, 即将为您安装 aria2';
        apt-get install aria2;
    fi
    # 安装 nvm
    if ! type  nvm >/dev/null 2>&1; then
        echo '检测到系统没有安装 nvm, 即将为您安装 nvm';
        git clone ${nvm_path} ${home}/.nvm;
        # todo 把 nvm 的脚本导出到文件不用每次都 export
        sh ${home}/.nvm/nvm.sh
        export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
    fi
    # 安装 node
    if ! type  node >/dev/null 2>&1; then
        echo '检测到系统没有安装 node, 即将为您安装 node';
        nvm install latest;
    fi
    # 安装 nrm
    if ! type  nrm >/dev/null 2>&1; then
        echo '检测到系统没有安装 nrm, 即将为您安装 nrm';
        npm i -g nrm --registry=https://registry.npm.taobao.org
        nrm use taobao
    fi
    # 安装 pm2 进程守护
    if ! type  pm2 >/dev/null 2>&1; then
        echo '检测到系统没有安装 pm2, 即将为您安装 pm2';
        npm i -g pm2 --registry=https://registry.npm.taobao.org
    fi
    # 安装 shell-ui 脚本管理
    if ! type  pm2 >/dev/null 2>&1; then
        echo '检测到系统没有安装  shell-ui 脚本管理, 即将为您安装  shell-ui';
        npm i -g shell-ui --registry=https://registry.npm.taobao.org
    fi
}
# 下载并解压
function download () {
    # 下载文件 采用 git 来保存, 方便同步配置信息

    # 复制项目文件到目标
    # cp -rf ./config.d/ ${home}/config.d/
    # cp -rf ./ariaNG/ ${home}/ariaNG/
    # cp -rf ./decode/ ${home}/decode/
    # cp -rf ./filebrowser/ ${home}/filebrowser/
    # cp -rf ./frp/ ${home}/frp/

    # 复制 pm2 启动文件到 目标目录
    sed "s#{{homePath}}#${home}#g" ecosystem.config.back.js > ${home}/ecosystem.config.js
    # 复制 nginx 配置文件到 目标目录
    sed "s#{{homePath}}#${home}#g" ./nginx/ariaNG.back > ${home}/nginx/ariaNG.conf
    sed "s#{{homePath}}#${home}#g" ./nginx/decode.back > ${home}/nginx/decode.conf
}

# 安装依赖
checkDep
# 下载文件并将文件复制到对应路径
download
# 启动进程守护
cd ${home}
pm2 start
# dlna 启动一次就在 service 里面常驻了, 所以手动关闭一下
pm2 stop dlna
cd ${pwd}



